# -*- encoding: utf-8 -*-
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Eval
from trytond.model import ModelView, ModelSQL, fields
import csv
from trytond.i18n import gettext
from trytond.exceptions import UserError
from io import BytesIO


STATES = {
    'readonly': Eval('state') != 'draft',
}

__all__ = ['Inventory', 'InventoryLineImport']


class Inventory(metaclass=PoolMeta):
    __name__ = 'stock.inventory'

    import_lines = fields.One2Many('stock.inventory.line.import', 'inventory', 'Import lines', states=STATES)
    filename = fields.Char('Filename', readonly=True)
    inventory_file = fields.Binary('Import File', filename='filename', states=STATES)

    @classmethod
    def __setup__(cls):
        super(Inventory, cls).__setup__()
        cls._buttons.update({
                'import_file': {
                    'readonly': ~Eval('inventory_file') | (STATES['readonly'])
                    },
                'create_inventory_lines': {
                    'readonly': ~Eval('import_lines') | (STATES['readonly'])
                    }
                })

    @classmethod
    @ModelView.button
    def import_file(cls, inventories):
        """
        Read a csv file and create an InventoryLineImport object for each file row.
        Is needed that the file have a header with 7 columns referent to code, reference, description, lot, quantity, uom
        and location in this spelling.
        """
        for inventory in inventories:
            to_create = []
            data = BytesIO(inventory.inventory_file)
            for row in csv.DictReader(data, delimiter=';'):
                row['code'] = row['code'].decode('latin1')
                row['description'] = row['description'] .decode('latin1')
                row['reference'] = row['reference'].decode('latin1')
                row['inventory'] = inventory
                to_create.append(row)
            InventoryLineImport.create(to_create)

    @classmethod
    @ModelView.button
    def create_inventory_lines(cls, inventories):
        """
        Creates an inventory line for each InventoryLineImport object linked if there are no lines with an error state.
        In addition, it creates the lot if doesn't exists.
        """
        pool = Pool()
        InventoryLine = pool.get('stock.inventory.line')
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')
        to_create = []
        for inventory in inventories:
            for line in inventory.import_lines:
                if 'Error' in line.state:
                    raise UserError(gettext('electrans_stock_inventory.cannot_import'))
                product, = Product.search(['OR',
                                           [('code', '=', line.code), ('code', '!=', '')],
                                           [('reference', '=', line.reference), ('reference', '!=', '')]
                                           ], limit=1)
                if line.lot:
                    seq = product.template.lot_sequence
                    number = [seq.prefix + line.lot, line.lot] if seq else [line.lot]
                    lot = Lot.search([('number', 'in', number), ('product', '=', product.id)], limit=1)
                    lot, = lot if lot else Lot.create([{
                        'number': line.lot if seq and seq.prefix == line.lot[:len(seq.prefix)] else number[0],
                        'product': product.id}])

                to_create.append({'product': product, 'uom': product.template.default_uom.id,
                                  'lot': lot.id if line.lot else None,
                                  'quantity': line.quantity, 'inventory': inventory.id})
        InventoryLine.create(to_create)


class InventoryLineImport(ModelSQL, ModelView):
    "Inventory Import"
    __name__ = 'stock.inventory.line.import'
    _rec_name = 'description'

    inventory = fields.Many2One('stock.inventory', 'Inventory', required=True, ondelete='CASCADE')
    code = fields.Char('Code')
    reference = fields.Char('Reference')
    description = fields.Char('Description')
    lot = fields.Char('Lot')
    quantity = fields.Char('Quantity')
    uom = fields.Char('Uom')
    location = fields.Char('Location')
    state = fields.Function(fields.Char('State'), 'on_change_with_state')

    @fields.depends('code', 'lines', 'reference', 'reference', 'lot', 'uom', 'location')
    def on_change_with_state(self, name=None):
        # TODO: return the state text in the user language using translations.
        pool = Pool()
        Product = pool.get('product.product')
        Lot = pool.get('stock.lot')
        state = ""
        uom = self.uom if self.uom else "Unidad"
        domain = [('code', '=', self.code), ('code', '!=', '')] if self.code else []
        domain += [('reference', '=', self.reference), ('reference', '!=', '')] if self.reference else []

        product = Product.search(domain, limit=1)
        if not product or not domain:
            state = "Error: el producto no existe."
        elif product[0].template.type != 'goods':
            state = "Error: el producto no es un bien."
        elif not product[0].template.default_uom.name == uom:
            state = "Error: La unidad de medida no concuerda con la del producto."
        else:
            seq = product[0].template.lot_sequence

            number = [seq.prefix + self.lot, self.lot] if seq else [self.lot]
            if self.lot and not Lot.search([('number', 'in', number), ('product', '=', product[0].id)]):
                state = "Warning: El lote no existe y se creará automáticamente. "
            if self.inventory.location.name != self.location:
                state += "Warning: la ubicación no coincide con el inventario. "
        return state if state else "OK"
